﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableCube_DisappearAtContact : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if( other.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}
