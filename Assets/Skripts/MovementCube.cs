﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCube : MonoBehaviour
{
    Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //position = transform.position;
        //position.x += 0.1f;
        //transform.position = position;

        if(Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            transform.position = new Vector3(transform.position.x + Input.GetAxis("Horizontal") * Time.fixedDeltaTime,transform.position.y,transform.position.z + Input.GetAxis("Vertical")* Time.fixedDeltaTime);
        }

    }
}
