﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_BasicFollow : MonoBehaviour
{
    public GameObject playerObject;
    Transform associatedTransform;
    public float offset;
    public AudioClip acquireCube;
    public AudioClip cheersFoot;
    int tempCounter = 10;
    // Start is called before the first frame update
    void Start()
    {
        associatedTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(tempCounter != GameController.instance.numberOfCollectables && GameController.instance.numberOfCollectables > 1)
        {
            tempCounter = GameController.instance.numberOfCollectables;
            PlaySound(acquireCube);

        }else if(tempCounter != GameController.instance.numberOfCollectables && GameController.instance.numberOfCollectables == 0)
        {
            tempCounter = GameController.instance.numberOfCollectables;
            PlaySound(cheersFoot);
        }
        associatedTransform.position = new Vector3(playerObject.transform.position.x ,playerObject.transform.position.y+5, playerObject.transform.position.z-offset);
    }

    public static void PlaySound(AudioClip clipping)
    {
        AudioSource tempSource = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        tempSource.PlayOneShot(clipping);
    }
}
