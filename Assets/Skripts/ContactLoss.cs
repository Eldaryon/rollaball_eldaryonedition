﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactLoss : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && GameController.instance.numberOfCollectables > 0)
        {
            GameController.instance.fourPanels = true;
        }
    }
}
