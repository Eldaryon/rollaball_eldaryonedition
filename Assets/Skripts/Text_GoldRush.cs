﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_GoldRush : MonoBehaviour
{
    Animator associatedAnimator;
    bool triggered = false;
    public AudioClip oneleft;
    // Start is called before the first frame update
    void Start()
    {
        associatedAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameController.instance.numberOfCollectables == 1 && !triggered)
        {
            associatedAnimator.SetTrigger("GoldRush");
            triggered = true;
            Camera_BasicFollow.PlaySound(oneleft);
        }
    }
}
