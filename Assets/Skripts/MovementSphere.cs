﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSphere : MonoBehaviour
{
    Rigidbody associatedBody;
    [SerializeField]float fastest = 5f;
    // Start is called before the first frame update
    void Start()
    {
        // I can't find info about the Gyroscope for now, so let's just use the Accelerometer
        //Input.gyro.enabled = true;
        associatedBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameController.instance.numberOfCollectables > 0)
        {
            if(SystemInfo.deviceType == DeviceType.Desktop)
                associatedBody.AddForce(Input.GetAxis("Horizontal") * 5,0f,Input.GetAxis("Vertical") * 5,ForceMode.Acceleration);
            else if(SystemInfo.deviceType == DeviceType.Handheld)
                associatedBody.AddForce(Input.acceleration.x,0,Input.acceleration.y);
            if(associatedBody.velocity.magnitude > fastest)
            {
                associatedBody.velocity = associatedBody.velocity.normalized * fastest;
            }
        }
        
    }
}
