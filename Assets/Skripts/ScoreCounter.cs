﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreCounter : MonoBehaviour
{
    TextMeshProUGUI scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameController.instance.fourPanels)
        {
            if(GameController.instance.numberOfCollectables > 0)
            {   
            scoreText.text = "Qubes left - " + GameController.instance.numberOfCollectables;
            if(GameController.instance.numberOfCollectables > 1)
                scoreText.text += "\n" + (GameController.instance.numberOfCollectables-1) + " till Gold Rush";
            }else
            {
            scoreText.text = "Congratulations!\nYou win!";
            }
        }else
        {
            scoreText.text = "Oh no!\nYou fell!";
        }
        
        
    }
}
