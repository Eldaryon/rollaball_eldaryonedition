﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int numberOfCollectables = 0;
    public static GameController instance;
    public bool fourPanels = false;
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }else if(instance != this)
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        numberOfCollectables = GameObject.FindGameObjectsWithTag("Collectable").Length;
    }
}
